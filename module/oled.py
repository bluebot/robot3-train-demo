import time
import ssd1306
from machine import I2C, Pin

# display
i2c = I2C(scl=Pin(2), sda=Pin(14)) # D4 D5
display = ssd1306.SSD1306_I2C(128, 64, i2c)
# flip screen
display.write_cmd(0xA0) # SEGREMAP
display.write_cmd(0xC0) # COMSCANINC

display.fill(0)
display.show()

display.fill(0)
display.text("Hello World",16,30,1)
display.show()

display.fill(0)
display.text("Hello World",16,56,1)
display.show()
from machine import Pin, PWM
import time

# beeper D7 GPIO13
beeper = PWM(Pin(13, Pin.OUT), freq=600, duty=0) 

beeper.duty(512) 
time.sleep_ms(2)
beeper.duty(0)
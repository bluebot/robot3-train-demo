from machine import Pin
import time

# touch sensor D8 GPIO15
button = Pin(15, Pin.IN)

print("Test button")
print("no press:1  press:0")
while True:
    print(button.value())
    time.sleep_ms(100)

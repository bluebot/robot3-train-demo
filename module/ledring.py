import neopixel
from machine import Pin

# D6 GPIO12
led = neopixel.NeoPixel(Pin(12), 8) # D6
led[0] = (255, 0, 0) # set to red, full brightness
led[1] = (0, 128, 0) # set to green, half brightness
led[2] = (0, 0, 64)  # set to blue, quarter brightness
led.write()
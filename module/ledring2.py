import neopixel
from machine import Pin
import time

# D6 GPIO12
led = neopixel.NeoPixel(Pin(12), 8) # D6

ledCount = 0
while True:
    if ledCount < 8:
        led[ledCount] = (0,0,255)
        ledCount += 1
    else:
        ledCount = 0
        for i in range(8):
            led[i] = (0,0,0)
    led.write()
    time.sleep(1)
import os
import math
import time
from util import display

class DataSet:
	def __init__(self):
		self.samples = []
		self.n1_samples = 0
		self.n0_samples = 0
		self.n_samples = 0

	def reset(self):
		self.samples.clear()
		self.n1_samples = 0
		self.n0_samples = 0
		self.n_samples = 0

	def show(self):
		display.fill(0)
		for s in self.samples:
			if s[2] == 1:
				c = 'x'
			else:
				c = 'o'
			col = int(s[0]*60)
			row = 64-int(s[1]*200)
			print(col,row)
			display.text(c,col,row,1)
			time.sleep_ms(100)
			display.show()

	def add(self,x1,x2,y):
		if(y == 1 and self.n1_samples <10):
			self.n1_samples +=1
			self.n_samples += 1
			self.samples.append([x1,x2,y])
			# print(x1,x2,",1")
		elif(y == 0 and self.n0_samples <10):
			self.n0_samples +=1
			self.n_samples += 1
			self.samples.append([x1,x2,y])
			# print(x1,x2,",0")

		if(self.n_samples < 20):
			return False
		else:
			# self.show()
			# time.sleep(10)
			return True

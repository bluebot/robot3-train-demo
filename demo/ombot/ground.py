# Ground
import os

class Ground:
	def __init__(self,display,ground_level=55):
		self.display = display
		
		# y coordinate of ground
		self.ground_level=ground_level

		# create road lines
		self.n_lines = 0
		self.lines = []
		self.create_lines()

	def create_lines(self,n_lines=10):
		self.n_lines=n_lines
		self.lines.clear()

		for l in range(self.n_lines):
			r = int.from_bytes(os.urandom(1),'big')
			self.lines.append([r%128, self.ground_level + r%8 + 2, r%10])

	def reset(self):
		self.create_lines(self.n_lines)

	def update(self, show_x):
		for l in range(self.n_lines): # out of screen
			if(self.lines[l][0] + self.lines[l][2] < show_x):
				r = int.from_bytes(os.urandom(1),'big')
				self.lines[l][0] += 127 + r%5 # new x: move right to another position
				self.lines[l][1] = self.ground_level + r%8 +2 # new y

	def show(self, show_x):
		self.display.hline(0,self.ground_level,127,1)
		for l in range(self.n_lines):
			self.display.hline(self.lines[l][0] - show_x, \
								self.lines[l][1], \
								self.lines[l][2],1)

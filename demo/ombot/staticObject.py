import os
import math
import time

class StaticObject:
	def __init__(self,display,x,y,width,height,id):
		self.display=display

		self.x=x
		self.y=y
		self.width=width
		self.height=height

		self.id=id

	def move(self,x,y):
		self.x=x
		self.y=y

	def update(self, show_x):
		if(self.x + self.width/2 < show_x): # out of screen
			r = int.from_bytes(os.urandom(1),'big')
			self.x += 300 + r%10 # move right to another position

	def show(self, show_x):
		disp_x = self.x - show_x - self.width//2
		disp_y = self.y - self.height//2
		self.display.fill_rect(disp_x, disp_y, self.width, self.height, 1)

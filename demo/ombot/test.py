import time
from window import *
from util import display,beep,led,measureLight,measureDist
from questionWindow import Button

class TestWindow(Window):
	def __init__(self):
		Window.__init__(self)

		self.distance = None
		self.light = None
		self.startTime = time.ticks_ms()
		self.ledCount = 0

		self.btn_quit = Button("Quit",90,55,40,12)
		self.btn_quit.cb_hold = self.cb_quit
		Window.add(self,self.btn_quit)

		Window.setDrawCB(self,self.cb_draw)
		Window.setIdleCB(self,self.cb_idle)
		Window.setHoldCB(self,self.cb_quit)
	
	def reset(self):
		self.distance = None
		self.light = None
		for i in range(8):
			led[i] = (0,0,0)
		led.write()

	def run(self):
		Window.run(self)

	def cb_quit(self):
		self.reset()
		Window.close(self)

	def cb_idle(self):
		self.distance = measureDist()
		self.light = measureLight()
		if time.ticks_ms() - self.startTime > 1000:
			if self.ledCount < 8:
				led[self.ledCount] = (0,0,255)
				self.ledCount += 1
			else:
				self.ledCount = 0
				for i in range(8):
					led[i] = (0,0,0)
			led.write()
			beep()
			self.startTime = time.ticks_ms()

	def cb_draw(self):
		display.text('Test',2,2,1)
		display.text('distance:',10,20,1)
		display.text(str(self.distance),90,20,1)
		display.text('light:',10,35,1)
		display.text(str(self.light),65,35,1)




import os
import math
import urandom

class Neuron:
	def __init__(self):
		self.reset()

	def reset(self):
		self.w1 = urandom.getrandbits(8)/255.0
		self.w2 = urandom.getrandbits(8)/255.0
		self.b = urandom.getrandbits(8)/255.0
		self.x1 = 0
		self.x2 = 0
		self.y = 0.0

	def active(self,x1,x2):
		self.x1 = x1
		self.x2 = x2

		# the large exp() will cause error "ValueError: math domain error", so need to limit s
		s =  self.w1 * self.x1 + self.w2 * self.x2 + self.b
		if(s > 10.0):
			s = 10.0
		elif(s < -10.0):
			s = -10.0

		self.y = 1/(1+math.exp(-s))
		return self.y

	def infer(self,x1,x2):
		self.active(x1,x2)
		return self.y>0.5

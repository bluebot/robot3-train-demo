import sys
import time
from util import measureLight,button,buttonDownLevel,display,drawImageFile,beeper

class ImageBox:
	def __init__(self):
		self.imgs=[]
		self.nums=0
		self.current = -1
	
	def draw(self):
		if self.current >= 0:
			drawImageFile(self.imgs[self.current],0,0)

	def nextItem(self):
		if self.nums > 0:
			self.current = (self.current + 1) % self.nums

	def addItem(self,img):
		self.imgs.append(img)
		self.nums += 1
		self.current += 1
	
	def reset(self):
		self.imgs.clear()
		self.current = -1
		self.nums = 0

tempo = 4
tones = {
	'1': 262,
	'2': 294,
	'3': 330,
	'4': 349,
	'5': 392,
	'6': 440,
	'7': 494,
	'8': 523,
	'0': 0}
melody = [1,1,5,5,6,6,5,0,4,4,3,3,2,2,1]

def runTone(count):
	if count == 7:
		beeper.duty(0)
		return
	beeper.duty(512)
	beeper.freq(tones[str(melody[count])])
	
def runIdle():
	beeper.duty(0)

class Show():
	def __init__(self):
		self.NORMAL = 0
		self.SLEEP1 = 1 # Sleep because of getting dark.
		self.WAKEUP = 3
		self.SINGING = 4
		self.state = self.NORMAL

		self.happy_face = ImageBox() # NORMAL
		self.happy_face.addItem('img/01.pb')
		self.happy_face.addItem('img/02.pb')
		self.happy_face.addItem('img/03.pb')
		self.happy_face.addItem('img/04.pb')

		self.sleep_face = ImageBox() # SLEEP
		self.sleep_face.addItem('img/05.pb')
		self.sleep_face.addItem('img/06.pb')
		self.sleep_face.addItem('img/07.pb')

		self.wake_face = ImageBox() #　WAKEUP
		self.wake_face.addItem('img/05.pb')
		self.wake_face.addItem('img/07.pb')

		self.sing_face = ImageBox() # SINGING
		self.sing_face.addItem('img/08.pb')
		self.sing_face.addItem('img/09.pb')
		self.sing_face.addItem('img/10.pb')

		self.face = self.happy_face

		self.start_time = self.switch_time = time.ticks_ms()
		self.IDLE_TIME = 10000
	
	def __del__(self):
		del(self.happy_face)
		del(self.sleep_face)
		del(self.wake_face)
		del(self.sing_face)
		del(self.face)

	def draw(self):
		display.fill(0)
		self.face.draw()
		display.show()

	def run(self):
		time.sleep(1)
		countTone=0
		frame = 0
		while(True):
			start = time.ticks_ms()
			# 读取传感器
			light = measureLight()
			pinValue = button.value()
			if pinValue == buttonDownLevel:
				return
			# 状态机决策
			if self.state == self.NORMAL:
				if light > 800:  # Getting dark
					self.state = self.SLEEP1
					self.sleep_face.current = 0 # showSleep()
					self.face = self.sleep_face
					self.start_time = self.switch_time = time.ticks_ms()
				elif time.ticks_ms() - self.switch_time >  1000: # Switch image
					self.happy_face.nextItem()
					self.switch_time = time.ticks_ms()
			elif self.state == self.SLEEP1:
				if light <= 800:
					self.state = self.WAKEUP
					self.wake_face.current = 0
					self.face = self.wake_face
					self.start_time = self.switch_time = time.ticks_ms()
				elif self.sleep_face.current != 2 and time.ticks_ms() - self.switch_time >= 1000:
					self.sleep_face.nextItem()
					self.switch_time = time.ticks_ms()
			elif self.state == self.WAKEUP:
				if time.ticks_ms() - self.start_time >= 2000:
					self.state = self.SINGING
					self.sing_face.current = 0
					self.face = self.sing_face
					self.start_time = self.switch_time = time.ticks_ms()
				elif time.ticks_ms() - self.switch_time >= 300:
					self.wake_face.nextItem()
					self.switch_time = time.ticks_ms()
			elif self.state == self.SINGING:
				if countTone == len(melody):
					countTone = 0
					self.state = self.NORMAL
					self.face = self.happy_face
					self.start_time = self.switch_time = time.ticks_ms()
				elif time.ticks_ms() - self.switch_time >= 500:
					self.sing_face.nextItem()
					self.switch_time = time.ticks_ms()
				if frame == 0:
					runTone(countTone)
					countTone += 1
					frame = not frame
				else:
					beeper.duty(0)
					frame= not frame

			# 屏幕显示
			self.draw()

			end = time.ticks_ms()
			time.sleep_ms(150-(end-start))
from window import *
from util import display

class Button:
	def __init__(self,title,cx,cy,w=20,h=12,focus=0):
		self.title = title
		self.cx = cx
		self.cy = cy
		self.w = w
		self.h = h
		self.focus = focus
		
		self.cb_idle = None
		self.cb_click = None
		self.cb_hold = None
		self.cb_holding = None
		self.cb_release = None
	
	def draw(self):
		display.text(self.title,self.cx-self.w//2,self.cy-self.h//2,1)
		if self.focus == 1:
			display.rect(self.cx-self.w//2-2,self.cy-self.h//2-2,self.w,self.h,1)

class QuestionWindow(Window):
	def __init__(self,title="Question"):
		Window.__init__(self)
		self.title = title
		self.value = True
		
		# add your component
		self.btn_yes = Button("Yes",40,50,35,12)
		self.btn_no = Button("No",90,50,55,12)
		Window.add(self,self.btn_yes)
		Window.add(self,self.btn_no)
		# add backgound draw
		Window.setDrawCB(self,self.draw)

		# add your callback
		self.btn_yes.cb_hold = self.cb_yes
		self.btn_no.cb_hold = self.cb_no
  
	def run(self):
		Window.run(self)
	
	def cb_yes(self):
		self.value = True
		Window.close(self)
	
	def cb_no(self):
		self.value = False
		Window.close(self) 
	
	def draw(self):
		display.text(self.title,10,20,1)

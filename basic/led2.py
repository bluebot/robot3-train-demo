from machine import Pin
import time

led = Pin(2, Pin.OUT)
sleeptime = 1

while True:
    led.value(1) 
    time.sleep(1)

    led.value(0) 
    time.sleep(sleeptime)
    print("sleeptime: ", sleeptime)
    if sleeptime < 3:
        sleeptime = sleeptime + 1
    else:
        sleeptime = 1

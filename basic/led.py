from machine import Pin
import time

led = Pin(2, Pin.OUT)

while True:
    led.value(0) # turn on led
    time.sleep(1)

    led.value(1) # turn off led
    time.sleep(1)
